use crate::*;

impl Notification {
    pub fn signal(&self) -> Result<(), Error> {
        self.0.send_nb(0usize, &[], &[]).map(|_| ())
    }

    pub fn poll(&self) -> Result<usize, Error> {
        self.0
            .wait_nb(0usize, &mut [0usize], &mut [])
            .map(|(data_out, _)| data_out[0])
    }

    pub fn wait(&self) -> Result<usize, Error> {
        self.0
            .wait(0usize, &mut [0usize], &mut [])
            .map(|(data_out, _)| data_out[0])
    }
}
