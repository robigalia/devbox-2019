use crate::Error;

/// An unowned capability pointer. Use `Cap` for owned handles.
#[derive(Clone, Copy)]
pub struct CPtr(crate usize);
/// An owned CPtr. Unsafe to construct and safe to invoke.
pub struct Cap(CPtr);

impl Cap {
    pub unsafe fn from_cptr(cptr: CPtr) -> Self {
        Cap(cptr)
    }

    pub fn as_cptr(&self) -> CPtr {
        self.0
    }

    // blocking
    // send + recv
    #[inline]
    pub fn call(
        &self,
        _label: usize,
        _data_in: &[usize],
        _cap_in: &[CPtr],
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    // send_nb + recv
    #[inline]
    pub fn reply_recv(
        &self,
        _label: usize,
        _data_in: &[usize],
        _cap_in: &[CPtr],
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn send(&self, _label: usize, _data_in: &[usize], _cap_in: &[CPtr]) -> Result<(), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn recv(
        &self,
        _label: usize,
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn wait(
        &self,
        _label: usize,
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    // non-blocking
    #[inline]
    pub fn send_nb(
        &self,
        _label: usize,
        _data_in: &[usize],
        _cap_in: &[CPtr],
    ) -> Result<(), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn recv_nb(
        &self,
        _label: usize,
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn send_recv_nb(
        &self,
        _label: usize,
        _data_in: &[usize],
        _cap_in: &[CPtr],
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn send_wait_nb(
        &self,
        _label: usize,
        _data_in: &[usize],
        _cap_in: &[CPtr],
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }

    #[inline]
    pub fn wait_nb(
        &self,
        _label: usize,
        _data_out: &mut [usize],
        _cap_out: &mut [CPtr],
    ) -> Result<(&[usize], &[CPtr]), Error> {
        unimplemented!()
    }
}
