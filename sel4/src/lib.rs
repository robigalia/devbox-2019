#![feature(
    asm,
    doc_cfg,
    crate_visibility_modifier,
    const_raw_ptr_to_usize_cast,
    const_raw_ptr_deref,
    llvm_asm
)]
#![no_std]
#![cfg_attr(feature = "cargo-clippy", warn(unsafe_code))]

#[cfg(not(target_os = "sel4"))]
compile_error!("target_os /= sel4. Did you forget --target x86_64-sel4-robigalia?");

#[cfg(not(target_arch = "x86_64"))]
compile_error!("target_arch /= x86_64. Sorry, this sel4 crate only supports x86_64 for now.");

#[allow(
    clippy::needless_pass_by_value,
    clippy::unused_unit,
    clippy::too_many_arguments
)]
mod generated {
    use crate::*;

    include!(concat!(env!("OUT_DIR"), "/generated.rs"));
}

mod cap;
mod debug;
mod endpoint;
mod notification;
mod syscalls;
pub use crate::cap::{CPtr, Cap};
pub use crate::debug::Debug;
pub use crate::generated::*;

macro_rules! cap_wrapper {
    ($($tt:tt),* $(,)*) => {
        $(
            pub struct $tt(Cap);

            impl AsRef<Cap> for $tt {
                fn as_ref(&self) -> &Cap {
                    &self.0
                }
            }

            impl From<Cap> for $tt {
                fn from(cap: Cap) -> Self {
                    $tt(cap)
                }
            }
        )*
    }
}

cap_wrapper!(Notification, Endpoint);

// RENAME: IRQHandler::ack = acknowledge
// RENAME: X86IOPort::in8 = read8
// RENAME: X86IOPort::in16 = read16
// RENAME: X86IOPort::in32 = read32
// RENAME: X86IOPort::out8 = write8
// RENAME: X86IOPort::out16 = write16
// RENAME: X86IOPort::out32 = write32

impl TCB {
    // OVERRIDE: TCB::read_registers
    // Convenience: assume all registers are copied
    pub fn read_registers(
        &self,
        suspend_source: bool,
        arch_flags: u8,
    ) -> Result<UserContext, Error> {
        unimplemented!()
        //      self.read_registers_internal(suspend_source, arch_flags, UserContext::DATA_SIZE)
    }

    // OVERRIDE: TCB::write_registers
    // Convenience: assume all registers are copied
    pub fn write_registers(
        &self,
        suspend_source: bool,
        arch_flags: u8,
        user_context: UserContext,
    ) -> Result<(), Error> {
        unimplemented!()
        //      self.write_registers_internal(
        //          suspend_source,
        //          arch_flags,
        //          UserContext::DATA_SIZE,
        //          user_context,
        //      )
    }

    // OVERRIDE-: TCB::get_breakpoint
    // OVERRIDE-: TCB::set_breakpoint
}

#[cfg(feature = "KernelIsMCS")]
impl SchedContext {
    // OVERRIDE: SchedContext::bind
    pub fn bind(
        &self,
        // WEAK: should only accept TCB or Notification
        cap: impl AsRef<Cap>,
    ) -> Result<(), Error> {
        unimplemented!()
        //      self.bind_internal(cap.as_ref().as_cptr())
    }

    // OVERRIDE: SchedContext::unbind_object
    pub fn unbind_object(
        self,
        // WEAK: should only accept TCB or Notification
        cap: impl AsRef<Cap>,
    ) -> Result<(), Error> {
        unimplemented!()
        //      self.unbind_object_internal(cap.as_ref().as_cptr())
    }
}

// OVERRIDE-: CNode??
// https://gitlab.com/robigalia/rust-sel4/blob/master/src/cspace.rs
// Use SlotRef to wrap (root, guard, depth)

#[cfg(any(feature = "root-task", rustdoc))]
pub mod bootinfo {
    use crate::*;

    pub struct RootCaps {
        pub null: Cap,
        pub init_thread_tcb: TCB,
        pub init_thread_cnode: CNode,
        pub init_thread_vspace: X64PML4,
        pub irq_control: IRQControl,
        pub asid_control: X86ASIDControl,
        pub init_thread_asid_pool: X86ASIDPool,
        pub ioport_control: X86IOPortControl,
        pub iospace: X86IOSpace,
        pub boot_info_frame: X86Page,
        pub init_thread_ipc_buffer: X86Page,
        pub domain: DomainSet,
        // pub init_thread_scheduler: SchedContext,
        pub num_initial_caps: usize,
        pub debug: debug::Debug,
        //  pub bootinfo: &'static mut Bootinfo,
    }

    use core::sync::atomic::{AtomicBool, Ordering, ATOMIC_BOOL_INIT};

    static TAKEN_ROOT_CAPS: AtomicBool = ATOMIC_BOOL_INIT;

    #[cfg(feature = "internal-untake-root-caps")]
    pub unsafe fn untake_root_caps(_: RootCaps) {
        assert!(
            TAKEN_ROOT_CAPS.swap(false, Ordering::Relaxed),
            "Root caps had not yet been taken!",
        );
    }

    pub fn take_root_caps() -> RootCaps {
        assert!(
            !TAKEN_ROOT_CAPS.swap(true, Ordering::Relaxed),
            "Root caps have already been taken!",
        );

        unsafe {
            RootCaps {
                null: Cap::from_cptr(CPtr(0)),
                init_thread_tcb: TCB::from(Cap::from_cptr(CPtr(1))),
                init_thread_cnode: CNode::from(Cap::from_cptr(CPtr(2))),
                init_thread_vspace: X64PML4::from(Cap::from_cptr(CPtr(3))),
                irq_control: IRQControl::from(Cap::from_cptr(CPtr(4))),
                asid_control: X86ASIDControl::from(Cap::from_cptr(CPtr(5))),
                init_thread_asid_pool: X86ASIDPool::from(Cap::from_cptr(CPtr(6))),
                ioport_control: X86IOPortControl::from(Cap::from_cptr(CPtr(7))),
                iospace: X86IOSpace::from(Cap::from_cptr(CPtr(8))),
                boot_info_frame: X86Page::from(Cap::from_cptr(CPtr(9))),
                init_thread_ipc_buffer: X86Page::from(Cap::from_cptr(CPtr(10))),
                domain: DomainSet::from(Cap::from_cptr(CPtr(11))),
                //  init_thread_scheduler: SchedContext::from(Cap::from_cptr(CPtr(12))),
                num_initial_caps: 13,
                debug: debug::Debug,
            }
        }
    }
}
