#![allow(dead_code, clippy::too_many_arguments)]

use core::{mem, slice};

use crate::generated::IDLGENSyscallNumbers;
use crate::CPtr;

enum GS {}
impl GS {
    #[inline]
    unsafe fn read_ty<T>(offset: usize, buf: &mut T) {
        assert_eq!(mem::align_of::<T>(), mem::align_of::<usize>());
        let slice = slice::from_raw_parts_mut(
            buf as *mut T as *mut usize,
            mem::size_of_val(buf) / mem::size_of::<usize>(),
        );
        GS::read(offset, slice);
    }

    #[inline]
    unsafe fn write_ty<T>(offset: usize, buf: &T) {
        assert_eq!(mem::align_of::<T>(), mem::align_of::<usize>());
        let slice = slice::from_raw_parts(
            buf as *const T as *const usize,
            mem::size_of_val(buf) / mem::size_of::<usize>(),
        );
        GS::write(offset, slice);
    }

    #[inline]
    unsafe fn read(offset: usize, buf: &mut [usize]) {
        // FIXME: use LLVM-IR for optimized @llvm.memcpy from address spaces 256 to 0
        assert!(offset + buf.len() * 8 <= 256);
        for (i, word) in buf.iter_mut().enumerate() {
            llvm_asm!("movq %gs:${1:c}, $0"
                    : "=r"(*word)
                    : "ri"(offset + i * 8)
                    :
                    : "volatile"
            );
        }
    }

    #[inline]
    unsafe fn write(offset: usize, buf: &[usize]) {
        // FIXME: use LLVM-IR for optimized @llvm.memcpy from address spaces 0 to 256
        assert!(offset + buf.len() * 8 <= 256);
        for (i, word) in buf.iter().enumerate() {
            llvm_asm!("movq $0, %gs:${1:c}"
                :
                : "ri"(*word), "ri"(offset + i * 8)
                : "memory"
                : "volatile"
            );
        }
    }
}

#[inline(always)]
pub(crate) fn x64_sys_send(
    sys: IDLGENSyscallNumbers,
    dest: CPtr,
    info: usize,
    mr1: usize,
    mr2: usize,
    mr3: usize,
    mr4: usize,
) {
    unsafe {
        llvm_asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              :
              : "{rdx}" (sys as usize),
                "{rdi}" (dest.0),
                "{rsi}" (info),
                "{r10}" (mr1),
                "{r8}" (mr2),
                "{r9}" (mr3),
                "{r15}" (mr4)
              : "rcx", "rbx", "r11"
              : "volatile");
    }
}

#[inline(always)]
pub(crate) fn x64_sys_send_null(sys: IDLGENSyscallNumbers, dest: CPtr, info: usize) {
    unsafe {
        llvm_asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              :
              : "{rdx}" (sys as usize),
                "{rdi}" (dest.0),
                "{rsi}" (info)
              : "rcx", "rbx", "r11"
              : "volatile");
    }
}

#[inline(always)]
pub(crate) fn x64_sys_recv(
    sys: IDLGENSyscallNumbers,
    src: CPtr,
    out_badge: &mut usize,
    out_info: &mut usize,
    out_mr1: &mut usize,
    out_mr2: &mut usize,
    out_mr3: &mut usize,
    out_mr4: &mut usize,
    reply: usize,
) {
    unsafe {
        llvm_asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              : "={rsi}" (*out_info),
                "={r10}" (*out_mr1),
                "={r8}" (*out_mr2),
                "={r9}" (*out_mr3),
                "={r15}" (*out_mr4),
                "={rdi}" (*out_badge)
              : "{rdx}" (sys as usize),
                "{rdi}" (src.0),
                "{r12}" (reply)
              : "memory", "rcx", "rbx", "r11"
              : "volatile");
    }
}

#[inline(always)]
pub(crate) fn x64_sys_send_recv(
    sys: IDLGENSyscallNumbers,
    dest: CPtr,
    out_dest: &mut CPtr,
    info: usize,
    out_info: &mut usize,
    in_out_mr1: &mut usize,
    in_out_mr2: &mut usize,
    in_out_mr3: &mut usize,
    in_out_mr4: &mut usize,
    reply: usize,
) {
    unsafe {
        llvm_asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              : "={rsi}" (*out_info),
                "={r10}" (*in_out_mr1),
                "={r8}" (*in_out_mr2),
                "={r9}" (*in_out_mr3),
                "={r15}" (*in_out_mr4),
                "={rdi}" (out_dest.0)
              : "{rdx}" (sys as usize),
                "{rsi}" (info),
                "{r10}" (*in_out_mr1),
                "{r8}" (*in_out_mr2),
                "{r9}" (*in_out_mr3),
                "{r15}" (*in_out_mr4),
                "{r12}" (reply),
                "{rdi}" (dest.0)
              : "memory", "rcx", "rbx", "r11"
              : "volatile");
    }
}

#[inline(always)]
pub(crate) fn x64_sys_nbsend_recv(
    sys: IDLGENSyscallNumbers,
    dest: CPtr,
    src: CPtr,
    out_dest: &mut CPtr,
    info: usize,
    out_info: &mut usize,
    in_out_mr1: &mut usize,
    in_out_mr2: &mut usize,
    in_out_mr3: &mut usize,
    in_out_mr4: &mut usize,
    reply: usize,
) {
    unsafe {
        llvm_asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              : "={rsi}" (*out_info),
                "={r10}" (*in_out_mr1),
                "={r8}" (*in_out_mr2),
                "={r9}" (*in_out_mr3),
                "={r15}" (*in_out_mr4),
                "={rdi}" (out_dest.0)
              : "{rdx}" (sys as usize),
                "{rdi}" (src.0),
                "{rsi}" (info),
                "{r10}" (*in_out_mr1),
                "{r8}" (*in_out_mr2),
                "{r9}" (*in_out_mr3),
                "{r15}" (*in_out_mr4),
                "{r12}" (reply),
                "{r13}" (dest.0)
              : "memory", "rcx", "rbx", "r11"
              : "volatile");
    }
}

#[inline(always)]
pub(crate) fn x64_sys_null(sys: IDLGENSyscallNumbers) {
    unsafe {
        llvm_asm!("movq %rsp, %rbx
              syscall
              movq %rbx, %rsp"
              :
              : "{rdx}" (sys as usize)
              : "rcx", "rbx", "r11", "rsi", "rdi"
              : "volatile");
    }
}
