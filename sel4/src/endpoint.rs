use crate::*;

// A reply should borrow the endpoint to prevent receiving without explicitly deciding to either
// invoke, drop, or stash a reply capability.
impl Endpoint {}
