mod build_idl;

use std::collections::{HashMap, HashSet};
use std::io::Write;
use std::{env, fs, io, path, process};

use string_morph::Morph;

use proc_macro2::{Ident, Span, TokenStream};
use quote::*;

fn main() {
    //
    // Load our files and directories from environment variables and/or default locations
    // and verify that they exist and are valid.
    //

    let cargo_manifest = env::var("CARGO_MANIFEST_DIR")
        .map(path::PathBuf::from)
        .expect("Expected CARGO_MANIFEST_DIR, are you using cargo?");
    eprintln!("cargo_manifest: {}", cargo_manifest.display());

    assert!(
        cargo_manifest.join("sel4").join("libsel4").exists(),
        "Could not find seL4 kernel source at {:?}. Did you forget to git clone recursively? Try: git submodule update --init --recursive",
        cargo_manifest.join("sel4")
    );

    let outdir = env::var("OUT_DIR")
        .map(path::PathBuf::from)
        .expect("expected OUT_DIR, are you using cargo?");
    eprintln!("outdir: {}", outdir.display());

    let sel4_build_dir = outdir.join("build");
    eprintln!("sel4_build_dir: {}", sel4_build_dir.display());

    // The seL4 build dir might exist from a previous compilation. If we don't clear those out we
    // could confuse ourselves.
    if sel4_build_dir.exists() {
        fs::remove_dir_all(&sel4_build_dir).expect("Could not delete sel4 build directory");
    }

    fs::create_dir(&sel4_build_dir).expect("Could not create sel4 build directory");

    let kernel_config = env::var("SEL4_CONFIG").map(path::PathBuf::from).unwrap_or(
        cargo_manifest
            .join("sel4")
            .join("configs")
            .join("config.cmake"),
    );
    eprintln!("kernel_config: {}", kernel_config.display());

    assert!(
        kernel_config.exists(),
        "Could not find seL4 kernel config at {:?}, please create it or pass the path to your kernel config in the SEL4_CONFIG env variable",
        kernel_config
    );

    let cmake_toolchain_file = env::var("CMAKE_TOOLCHAIN_FILE")
        .map(path::PathBuf::from)
        .unwrap_or(cargo_manifest.join("sel4").join("gcc.cmake"));
    eprintln!("cmake_toolchain_file: {}", cmake_toolchain_file.display());

    assert!(
        cmake_toolchain_file.exists(),
        "Could not find cmake toolchain file at {:?}, please create it or pass the path in the CMAKE_TOOLCHAIN_FILE env variable",
        cmake_toolchain_file
    );

    let sel4_version_file = cargo_manifest.join("sel4").join("VERSION");
    eprintln!("sel4_version_file: {}", sel4_version_file.display());

    assert!(
        sel4_version_file.exists(),
        "Could not find seL4 kernel version file at {:?}",
        sel4_version_file
    );

    let sel4_version =
        fs::read_to_string(&sel4_version_file).expect("Could not read seL4 kernel version file");
    eprintln!("sel4_version: {}", sel4_version);

    let sel4_idl_file = cargo_manifest
        .join("idl")
        .join(format!("sel4-{}.xml", &sel4_version));
    eprintln!("sel4_idl_file: {}", sel4_idl_file.display());

    assert!(
        sel4_idl_file.exists(),
        "Could not find seL4 IDL file for this sel4 version at {:?}, please generate it with https://gitlab.com/robigalia/sel4-idlgen",
        sel4_idl_file
    );

    //
    // Tell cargo to watch important environment variables and files and rerun the build if they change
    //

    println!("cargo:rerun-if-env-changed=SEL4_CONFIG");
    println!("cargo:rerun-if-env-changed=CMAKE_TOOLCHAIN_FILE");
    println!("cargo:rerun-if-changed={}", kernel_config.display());
    println!("cargo:rerun-if-changed={}", cmake_toolchain_file.display());
    println!("cargo:rerun-if-changed={}", sel4_version_file.display());
    println!("cargo:rerun-if-changed={}", sel4_idl_file.display());

    //
    // Configure and build seL4
    //

    // configure the kernel
    let cmake_exec = env::var("CMAKE").unwrap_or("cmake".to_owned());
    let status = process::Command::new(&cmake_exec)
        .current_dir(&sel4_build_dir)
        .arg(format!(
            "-DCMAKE_TOOLCHAIN_FILE={}",
            cmake_toolchain_file.display()
        ))
        .arg("-G")
        .arg("Ninja")
        .arg("-C")
        .arg(&kernel_config)
        .arg(cargo_manifest.join("sel4/"))
        .status()
        .expect("seL4 kernel configure failed");

    assert!(status.success(), "seL4 kernel configure failed");

    // build the kernel
    let ninja_exec = env::var("NINJA").unwrap_or("ninja".to_owned());
    let status = process::Command::new(&ninja_exec)
        .current_dir(&sel4_build_dir)
        .arg("kernel.elf")
        .status()
        .expect("sel4 kernel build failed");

    assert!(status.success(), "seL4 kernel build failed");

    //
    // Open the output file that we will store all of our generated code into
    //

    let mut out = fs::File::create(outdir.join("generated.rs"))
        .map(io::BufWriter::new)
        .expect("failed to open file out/generated.rs");

    //
    // Read the generated seL4 kernel configuration C #defines and:
    // - Store them as constants in our generated code
    // - Set corresponding rust feature flags
    //

    let mut bug_dup_wcet_scale = false;
    for line in fs::read_to_string(
        sel4_build_dir
            .join("gen_config")
            .join("kernel")
            .join("gen_config.h"),
    )
    .expect("failed to read gen_config.h")
    .lines()
    .map(|l| l.trim_start_matches("#define "))
    {
        let space = line.find(" ").expect("failed to find space!");
        let name = &line[..space];
        let value = &line[space + 1..];
        let iname = Ident::new(name, Span::call_site());

        // add key=value to the environment
        println!(r#"cargo:rustc-cfg=feature="{}={}""#, name, value);

        if name == "CONFIG_KERNEL_WCET_SCALE" {
            if bug_dup_wcet_scale {
                continue;
            }
            bug_dup_wcet_scale = true;
        }

        // FIXME: This will fail when compiling for a 64-bit platform an a 32-bit system
        if let Ok(int) = value.parse::<usize>() {
            write(&mut out, quote!( pub const #iname: usize = #int; ));

            // probably 1 = true; if not, oh well
            if int == 1 {
                println!(r#"cargo:rustc-cfg=feature="{}""#, name);
            }

            // add special cases for any integer comparisons
            match name {
                "CONFIG_MAX_NUM_NODES" if int > 0 => {
                    println!(r#"cargo:rustc-cfg=feature="CONFIGX_ENABLE_SMP_SUPPORT""#);
                }
                _ => (),
            }
        } else {
            write(&mut out, quote!( pub const #iname: &str = #value; ));
        }
    }

    //
    // lib.rs contains some special notation in comments that affect our code generation behavior.
    // This allows lib.rs to override code that we generate with a hand-written version. Note that
    // we still do code generation for overidden things, we just give our generated version an
    // internal name and export the hand-written version. The hand-written version thus still has
    // access to the generated code and can wrap it.
    //

    let lib_rs = fs::read_to_string(cargo_manifest.join("src").join("lib.rs"))
        .expect("failed to read src/lib.rs");
    let overrides = lib_rs
        .lines()
        .map(|l| l.trim())
        .filter_map(|line| match line {
            _ if line.starts_with("// OVERRIDE: ") => {
                let from = line.trim_start_matches("// OVERRIDE: ");
                let to = format!(
                    "{}_internal",
                    &from.split_at(from.find("::").unwrap()).1[2..]
                );
                Some((from, to))
            }
            _ if line.starts_with("// RENAME: ") => {
                let tmp = line.trim_start_matches("// RENAME: ");
                let (from, to) = tmp.split_at(tmp.find("=").unwrap());
                Some((from.trim(), to[1..].trim().to_owned()))
            }
            _ => None,
        })
        .collect::<HashMap<_, _>>();

    //
    // Generate code from the seL4 IDL file generated by https://gitlab.com/robigalia/sel4-idlgen
    //

    for entry in build_idl::Sel4Idl::new(
        &fs::read_to_string(sel4_idl_file).expect("Could not read seL4 idl file"),
    )
    .eval(&sel4_build_dir)
    {
        match entry {
            build_idl::Entry::Constant { name, value } => {
                write(
                    &mut out,
                    match value {
                        build_idl::Value::Uint(v) => format!(
                            "pub const {}: usize = {};",
                            rustify_name(name).to_uppercase(),
                            v
                        ),
                        build_idl::Value::Sint(v) => format!(
                            "pub const {}: usize = {}isize as usize;",
                            rustify_name(name).to_uppercase(),
                            v
                        ),
                        build_idl::Value::Null => panic!(
                            "Null value found for constant '{}' while evaluating IDL file",
                            name
                        ),
                        build_idl::Value::Undefined => panic!(
                            "Undefined value found for constant '{}' while evaluating IDL file",
                            name
                        ),
                    },
                );
            }
            build_idl::Entry::Enum { name, constants } => {
                let id_enum_name = ident(&rustify_type(name));
                let mut previous_values = HashSet::new();
                let mut overflow = TokenStream::new();

                let constants = constants.iter().map(|c| {
                    let id_name = ident(&rustify_type(c.name));

                    let expr_value = match c.value {
                        // FIXME: This will fail when compiling for a 32-bit platform
                        build_idl::Value::Uint(v) => quote!( #v as usize ),
                        build_idl::Value::Sint(v) => quote!( #v as usize ),
                        build_idl::Value::Null => panic!(
                            "Null value found for enum '{}' constant '{}' while evaluating IDL file",
                            name, c.name,
                            ),
                        build_idl::Value::Undefined => panic!(
                            "Undefined value found for enum '{}' constant '{}' while evaluating IDL file",
                            name, c.name,
                            ),
                    };

                    let v_u64 = c.value.as_u64().unwrap();

                    if previous_values.contains(&v_u64) {
                        // HACK: C enums can contain multiple constants with the same value, but
                        // rust enums cannot. If we encounter this situation, the enum constant
                        // will be promoted to a pub const, at least until we figure out a better
                        // way to handle this.
                        overflow.extend(quote!( pub const #id_name : usize = #expr_value; ));
                        quote!()
                    } else {
                        previous_values.insert(v_u64);
                        quote!( #id_name = #expr_value, )
                    }
                });

                write(
                    &mut out,
                    quote!(
                        #[repr(usize)]
                        pub enum #id_enum_name {
                            #( #constants )*
                        }
                    ),
                );

                write(&mut out, overflow);
            }
            build_idl::Entry::Type { name, type_ } => {
                let id_name = ident(&rustify_type(name));
                let id_type = ident(&rustify_type(type_));

                if type_ == "seL4_CPtr" {
                    write(
                        &mut out,
                        quote!(
                            pub struct #id_name(Cap);

                            impl AsRef<Cap> for #id_name {
                                fn as_ref(&self) -> &Cap {
                                    &self.0
                                }
                            }

                            impl From<Cap> for #id_name {
                                fn from(c: Cap) -> Self {
                                    #id_name(c)
                                }
                            }
                        ),
                    );
                } else {
                    write(&mut out, quote!( pub type #id_name = #id_type; ));
                }
            }
            build_idl::Entry::Struct {
                name,
                is_packed,
                fields,
            } => {
                let id_name = ident(&rustify_type(name));
                let packed = if is_packed {
                    quote!( ,packed )
                } else {
                    quote!()
                };

                let fields = fields.iter().map(|field| {
                    let field_name = ident(&rustify_name(field.name));
                    match field.type_ {
                        build_idl::StructFieldType::Simple(t) => {
                            let field_type = ident(&rustify_type(t));
                            quote!( pub #field_name: #field_type, )
                        }
                        build_idl::StructFieldType::Pointer(_t) => {
                            // TODO FIXME: Properly handle pointers
                            quote!( pub #field_name: usize, )
                        }
                        build_idl::StructFieldType::Array(t, v) => {
                            let field_type = ident(&rustify_type(t));
                            let v = v.as_u64().expect(
                                "Fixed length array size in struct field has non-numeric value",
                            );
                            // FIXME: This will fail when compiling for a 32-bit platform
                            quote!( pub #field_name: [#field_type; #v as usize], )
                        }
                    }
                });

                write(
                    &mut out,
                    quote!(
                        #[repr(C #packed)]
                        pub struct #id_name {
                            #( #fields )*
                        }
                    ),
                );
            }
            build_idl::Entry::BitfieldStruct {
                name,
                fields: _fields,
            } => {
                // TODO FIXME: Implement bitfield structs
                write(&mut out, format!("pub struct {}; ", rustify_type(name)));
            }
            build_idl::Entry::TaggedUnion {
                union_name,
                tag_name: _tag_name,
                fields: _fields,
            } => {
                // TODO FIXME: Implement tagged unions

                // TODO FIXME: Tagged Union fields consist of names of bitfields that we have
                // already processed above. The IDL should probably be changed to pluck out the
                // bitfields that are part of tagged unions and put their actual definition as
                // a child to the tagged union so they can all be processed as a unit here where
                // they belong.

                write(
                    &mut out,
                    format!("pub struct {}; ", rustify_type(union_name)),
                );
            }
            build_idl::Entry::Syscall {
                name,
                number,
                does_send_ipcbuf: _does_send_ipcbuf,
                does_receive_ipcbuf: _does_receive_ipcbuf,
                arguments,
                return_values,
            } => {
                // TODO FIXME: Properly implement
                //     - generate variants that use the ipcbuffer
                //     - need to honor syscall/sysenter config flag on x64
                //     - support more architectures
                //     - calculate clobbers correctly, currently is too conservative

                let syscall_name = ident(&rustify_name(name));
                let syscall_number = number.as_i64().unwrap() as isize;

                let mut input_params = TokenStream::new();
                let mut input_asm_block = TokenStream::new();

                for param in &arguments {
                    let id_name = ident(&rustify_name(param.name));
                    // HACK just to see something work
                    let id_type;
                    if param.type_ == "seL4_MessageInfo_t" {
                        id_type = ident("usize");
                    } else {
                        id_type = ident(&rustify_type(param.type_));
                    }
                    let reg = format!("{{{}}}", param.register);
                    match param.special_value {
                        Some("sysno") => {
                            let id_sysno = ident("sysno");
                            if input_asm_block.is_empty() {
                                input_asm_block.extend(quote!( #reg (#id_sysno) ));
                            } else {
                                input_asm_block.extend(quote!( , #reg (#id_sysno) ));
                            }
                        }
                        None => {
                            input_params.extend(quote!( #id_name: #id_type, ));
                            if input_asm_block.is_empty() {
                                input_asm_block.extend(quote!( #reg (#id_name as usize) ));
                            } else {
                                input_asm_block.extend(quote!( , #reg (#id_name as usize) ));
                            }
                        }
                        Some(x) => panic!(
                            "Unknown special value '{}' for syscall '{}' argument '{}'",
                            x, name, param.name
                        ),
                    }
                }

                let mut output_params = TokenStream::new();
                let mut output_asm_block = TokenStream::new();
                let mut output_params_decl = TokenStream::new();
                let mut output_params_return = TokenStream::new();

                for param in &return_values {
                    let id_name = ident(&rustify_name(param.name));
                    // HACK just to see something work
                    let id_type;
                    if param.type_ == "seL4_MessageInfo_t" {
                        id_type = ident("usize");
                    } else {
                        id_type = ident(&rustify_type(param.type_));
                    }
                    let reg = format!("={{{}}}", param.register);

                    // TODO: Don't initialize these variables
                    output_params_decl.extend(quote!( let mut #id_name: #id_type = 0; ));

                    if output_params.is_empty() {
                        output_params.extend(quote!( #id_type ));
                        output_params_return.extend(quote!( #id_name ));
                        output_asm_block.extend(quote!( #reg (#id_name as #id_type) ));
                    } else {
                        output_params.extend(quote!( , #id_type ));
                        output_params_return.extend(quote!( , #id_name ));
                        output_asm_block.extend(quote!( , #reg (#id_name as #id_type) ));
                    }
                }

                output_params = match return_values.len() {
                    0 => quote!(),
                    1 => quote!( -> #output_params ),
                    _ => quote!( -> (#output_params) ),
                };

                output_params_return = match return_values.len() {
                    0 => quote!(),
                    1 => output_params_return,
                    _ => quote!( (#output_params_return) ),
                };

                write(
                    &mut out,
                    quote!(
                        #[inline(always)]
                        #[allow(unused_variables)]
                        pub fn #syscall_name(#input_params) #output_params {
                            // TODO: don't need a variable for sysno, just put it in the asm
                            let sysno = #syscall_number as usize;
                            #output_params_decl
                            unsafe {
                                llvm_asm!(
                                    "movq %rsp, %rbx
                                     syscall
                                     movq %rbx, %rsp"
                                    : #output_asm_block
                                    : #input_asm_block
                                    : "memory", "rcx", "rbx", "r11", "rsi", "rdi"
                                    : "volatile"
                                );
                            }
                            #output_params_return
                        }
                    ),
                );
            }
            build_idl::Entry::Interface {
                name: iface_name,
                methods,
            } => {
                let struct_name = ident(&rustify_type(iface_name));
                let methods = methods.iter().map(|method| {
                    let mut method_name = ident(&rustify_name(method.name));
                    let vis = match overrides.get(&*format!("{}::{}", struct_name, method_name)) {
                        Some(name) if name.ends_with("_internal") => {
                            method_name = ident(name);
                            quote!( pub(crate) )
                        }
                        Some(name) => {
                            method_name = ident(name);
                            quote!(pub)
                        }
                        None => quote!(pub),
                    };

                    let input_params = method.arguments.iter().map(|param| {
                        let name = ident(&rustify_name(param.name));
                        let type_ = ident(&rustify_type(param.type_));
                        quote!( #name: #type_, )
                    });

                    let mut output_params = TokenStream::new();
                    for param in &method.return_values {
                        let type_ = ident(&rustify_type(param.type_));
                        if output_params.is_empty() {
                            output_params.extend(quote!( #type_ ));
                        } else {
                            output_params.extend(quote!( , #type_ ));
                        }
                    }

                    output_params = match method.return_values.len() {
                        0 => quote!(()),
                        1 => quote!( #output_params ),
                        _ => quote!( (#output_params) ),
                    };

                    quote!(
                        #[inline(always)]
                        #[allow(unused_variables)]
                        #vis fn #method_name(&self, #( #input_params )* )
                        -> Result<#output_params, Error>
                        {
                            // FIXME:
                            //
                            // [ ] marshal input params
                            // [ ] invoke the capability (self.0) with the message
                            // [ ] unmarshal output params
                            unimplemented!()
                        }
                    )
                });

                write(
                    &mut out,
                    quote!(
                        impl #struct_name {
                            #( #methods )*
                        }
                    ),
                );
            }
            build_idl::Entry::Error { name, type_ } => {
                write(
                    &mut out,
                    format!(
                        "\n\n// TODO FIXME BUGBUG: IDL File is missing a definition for {}: {}\n\n",
                        type_, name,
                    ),
                );
            }
        }
    }

    //
    // Write files
    //

    // flush and close the file as we're about to ask rustfmt to reformat it.
    drop(out);

    // update the runner environment so it can find the kernel
    fs::write(
        cargo_manifest.join(".cargo").join("env"),
        format!("OUT_DIR={}", outdir.display()),
    )
    .expect("failed to write environment information for the runner");

    if let Err(_) = process::Command::new("rustfmt")
        .arg("generated.rs")
        .current_dir(outdir)
        .output()
    {
        println!(r#"cargo:warning="rustfmt is not installed. could not format generated.rs.""#);
    }
}

fn write(out: &mut impl Write, tokens: impl core::fmt::Display) {
    write!(out, "{}", tokens).unwrap();
}

fn ident(s: &str) -> Ident {
    Ident::new(s, Span::call_site())
}

fn rustify_type(s: &str) -> String {
    match s {
        "char" => return "i8".to_owned(),
        "signed char" => return "i8".to_owned(),
        "unsigned char" => return "u8".to_owned(),
        "signed short" => return "i16".to_owned(),
        "unsigned short" => return "u16".to_owned(),
        "signed int" => return "i32".to_owned(),
        "unsigned int" => return "u32".to_owned(),
        "signed long" => return "isize".to_owned(),
        "unsigned long" => return "usize".to_owned(),
        "signed long long" => return "i64".to_owned(),
        "unsigned long long" => return "u64".to_owned(),
        _ => {}
    };
    let s = s
        .trim_start_matches("seL4_")
        .trim_start_matches("SEL4_")
        .trim_end_matches("_t")
        .to_pascal_case();
    match &*s {
        // Correct case for all UPPERCASE things
        "Tcb" => "TCB",
        // x64
        "X64PmL4" => "X64PML4",
        "X64Pdpt" => "X64PDPT",
        // x86
        "X86Eptpd" => "X86EPTPD",
        "X86Eptpt" => "X86EPTPT",
        "X86Eptpdpt" => "X86EPTPDPT",
        "X86EptpmL4" => "X86EPTPML4",
        "X86Vcpu" => "X86VCPU",
        _ => &s,
    }
    .to_string()
}

fn rustify_name(s: &str) -> String {
    let s = s
        .trim_start_matches("seL4_")
        .trim_start_matches("SEL4_")
        .to_snake_case();
    let s = match &*s {
        // HACKS: use a complete list of rust keywords
        "move" => "move_",
        "type" => "type_",
        "yield" => "yield_",
        _ => &s,
    };
    if s.chars().nth(0).unwrap().is_digit(10) {
        let mut tmp = "_".to_string();
        tmp.push_str(s);
        tmp
    } else {
        s.to_string()
    }
}
