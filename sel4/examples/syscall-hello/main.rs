#![feature(core_intrinsics, core_panic_info, lang_items, global_asm)]
#![no_std]

use core::fmt;

// FIXME: support more than just x86_64!
mod x86_64_start;

struct DebugOutHandle;

impl fmt::Write for DebugOutHandle {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for b in s.as_bytes() {
            sel4::debug_put_char(*b as i8);
        }
        Ok(())
    }
}

macro_rules! debug_println {
    ($($tt:tt)*) => {{
        use core::fmt::Write;
        writeln!(DebugOutHandle, $($tt)*)
    }}
}

fn main() {
    debug_println!("");
    debug_println!("Hello world!");
    debug_println!("");
    debug_println!("Yes, the crash below is expected!");
    debug_println!("");
    debug_println!("To exit qemu, press: <control>+<a> <x>.");
    debug_println!("");
}
